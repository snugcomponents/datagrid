<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid;

enum OrderDirectionCase: string
{

	case ASC = 'ASC';
	case DESC = 'DESC';

}
