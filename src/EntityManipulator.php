<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid;

use Nette\Security\Resource;

interface EntityManipulator
{

	public function persist(Resource $resourceEntity): void;

	public function flush(Resource|null $resourceEntity = null): void;

	public function remove(Resource $resourceEntity): void;

}
