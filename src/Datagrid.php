<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid;

use ArrayAccess;
use Countable;
use Nette\Application\Attributes\Persistent;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Security\Resource;
use Nette\Utils\Arrays;
use Snugcomponents\Datagrid\Exception\Exception;
use Snugcomponents\Datagrid\Exception\ExceptionCase;
use Snugcomponents\Datagrid\InlineEditor\SetupBuilder;
use Snugcomponents\Datagrid\Item\ControlFactory;
use Snugcomponents\Filter\Filter;
use Snugcomponents\Filter\FilterFactory;
use Snugcomponents\Filter\FilterSetupBuilder;
use Snugcomponents\Paginator\Paginator;
use Snugcomponents\Paginator\PaginatorFactory;
use Traversable;
use function array_merge;
use function assert;

class Datagrid extends Control
{

	#[Persistent] public string|null $orderColumn = null;

	#[Persistent] public string|null $orderDirection = null;

	/** @var array<callable(Item\Control): void>  Occurs when remove signal is received */
	public array $onRemove = [];

	/** @var array<callable(Paginator): void>  Occurs when page signal is received */
	public array $onPagination = [];

	/** @var array<callable(Filter): void>  Occurs when filter signal is received (even if filter is cleared) */
	public array $onFiltering = [];

	/** @var array<callable(self): void>  Occurs when ordering signal is received */
	public array $onOrdering = [];

	/* @phpstan-ignore-next-line */
	/** @var array<callable(Form, array|object): void|callable(array|object): void>  Occurs when inline edit form success */
	public array $onInlineEditSuccess = []; /** @phpstan-ignore-line */

	/** @var array<string, mixed> */
	public array $ownTemplateArgs = [];

	private string|null $paginatorTemplateFilePath = null;

	private string|null $filterTemplateFilePath = null;

	private FilterSetupBuilder|null $filterSetupBuilder = null;

	private SetupBuilder|null $inlineEditorSetupBuilder = null;

	private string $templateFile = __DIR__ . '/Examples/templates/bootstrap4.latte';

	private string $inlineEditorTemplateFilePath = __DIR__ . '/Examples/templates/InlineEditor/bootstrap4.latte';

	private string $itemTemplateFilePath = __DIR__ . '/Examples/templates/Item/bootstrap4.latte';

	/** @var Countable&Traversable<Resource>&ArrayAccess<int|string, Resource> */
	private Traversable&Countable&ArrayAccess $items;

	public function __construct(
		private readonly FilterFactory $filterFactory,
		private readonly PaginatorFactory $paginatorFactory,
		private readonly ControlFactory $itemControlFactory,
		private readonly EntityManipulator $entityManipulator,
		private readonly DatagridDataBuilder $datagridDataBuilder,
		private readonly int $itemsPerPage,
	)
	{
	}

	public function setTemplateFile(string $templateFile): static
	{
		$this->templateFile = $templateFile;

		return $this;
	}

	public function setItemTemplateFile(string $templatePath): static
	{
		$this->itemTemplateFilePath = $templatePath;

		return $this;
	}

	public function setPaginatorTemplateFile(string $templatePath): static
	{
		$this->paginatorTemplateFilePath = $templatePath;

		return $this;
	}

	public function setFilterTemplateFile(string $templatePath): static
	{
		$this->filterTemplateFilePath = $templatePath;

		return $this;
	}

	public function setInlineEditorTemplateFile(string $templatePath): static
	{
		$this->inlineEditorTemplateFilePath = $templatePath;

		return $this;
	}

	public function setFilterSetupBuilder(FilterSetupBuilder $filterSetupBuilder): static
	{
		$this->filterSetupBuilder = $filterSetupBuilder;

		return $this;
	}

	public function setInlineEditorSetupBuilder(SetupBuilder $inlineEditorSetupBuilder): static
	{
		$this->inlineEditorSetupBuilder = $inlineEditorSetupBuilder;

		return $this;
	}

	public function render(): void
	{
		$template = $this->getTemplate();
		assert($template instanceof Template);

		$template->render($this->templateFile, [
			'orderColumn' => $this->orderColumn,
			'orderDirection' => $this->orderDirection,
			'isFilterSet' => (bool) $this->filterSetupBuilder,
			'items' => $this->getItems(),
			'dataBuilder' => $this->datagridDataBuilder,
			...$this->ownTemplateArgs,
		]);
	}

	public function createComponentPaginator(): Paginator
	{
		$paginator = $this->paginatorFactory->create($this->datagridDataBuilder, $this->itemsPerPage);
		$paginator->onPagination[] = $this->ajaxHandler(...);
		$paginator->onPagination = array_merge($paginator->onPagination, $this->onPagination);

		if ($this->paginatorTemplateFilePath) {
			$paginator->setTemplateFile($this->paginatorTemplateFilePath);
		}

		return $paginator;
	}

	public function createComponentFilter(): Filter
	{
		if (!$this->filterSetupBuilder) {
			throw new Exception(ExceptionCase::MissingFilterSetupBuilder);
		}

		$filter = $this->filterFactory->create($this->filterSetupBuilder, $this->datagridDataBuilder);
		$filter->onFiltering[] = $this->ajaxHandler(...);
		$filter->onFiltering[] = $this->resetPaginator(...);
		$filter->onFiltering = array_merge($filter->onFiltering, $this->onFiltering);

		if ($this->filterTemplateFilePath) {
			$filter->setTemplateFile($this->filterTemplateFilePath);
		}

		return $filter;
	}

	public function createComponentItem(): Multiplier
	{
		return new Multiplier(function (int|string $id): Control {
			/* @phpstan-ignore-next-line */
			$entity = $this->getItems()[$id] ?? $this->error('Entity does not exists.');
			assert($entity instanceof Resource);

			$item = $this->itemControlFactory->create(
				$entity,
				$this->entityManipulator,
				$this->itemTemplateFilePath,
				$this->inlineEditorTemplateFilePath,
				$this->inlineEditorSetupBuilder,
			);
			$item->onRemove[] = $this->ajaxHandler(...);
			$item->onRemove = array_merge($item->onRemove, $this->onRemove);
			$item->onInlineEditSuccess = array_merge($item->onInlineEditSuccess, $this->onInlineEditSuccess);

			return $item;
		});
	}

	public function handleOrderBy(): void
	{
		$this->ajaxHandler();
		Arrays::invoke($this->onOrdering);
	}

	private function applyOrder(): void
	{
		if (!$this->orderDirection || !$this->orderColumn) {
			return;
		}

		$direction = OrderDirectionCase::tryFrom($this->orderDirection);

		if (!$direction) {
			$this->error('Wrong direction');
		}

		/* @phpstan-ignore-next-line */
		$this->datagridDataBuilder->orderBy($this->orderColumn, $direction);
	}

	private function ajaxHandler(): void
	{
		$this->redrawControl('grid');
	}

	private function resetPaginator(): void
	{
		$paginator = $this->getComponent('paginator');
		assert($paginator instanceof Paginator);
		$paginator->page = 1;
	}

	/**
	 * @return Countable&Traversable<Resource>&ArrayAccess<int|string, Resource>
	 */
	private function getItems(): Traversable&Countable&ArrayAccess
	{
		if (!isset($this->items)) {
			if ($this->filterSetupBuilder) {
				$filter = $this->getComponent('filter');
				assert($filter instanceof Filter);
				$filter->applyFilter();
			}

			$this->applyOrder();

			$paginator = $this->getComponent('paginator');
			assert($paginator instanceof Paginator);
			$paginator->applyPaginator();

			$this->items = $this->datagridDataBuilder->build();
		}

		return $this->items;
	}

}
