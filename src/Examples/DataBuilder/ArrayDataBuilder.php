<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Examples\DataBuilder;

use ArrayAccess;
use Countable;
use Nette\Security\Resource;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;
use Nette\Utils\Paginator;
use Snugcomponents\Datagrid\DatagridDataBuilder;
use Snugcomponents\Datagrid\OrderDirectionCase;
use Traversable;
use function array_chunk;
use function array_filter;
use function count;
use function is_string;
use function max;
use function strcmp;
use function uasort;

class ArrayDataBuilder implements DatagridDataBuilder
{

	use SmartObject;

	/** @var array{age: int|null}|null */
	private array|null $filter = null;

	/** @var array{column: string, direction: OrderDirectionCase}|null */
	private array|null $sort = null;

	private Paginator|null $paginator = null;

	/**
	 * @param array<int|string, Resource> $data
	 */
	public function __construct(private array $data)
	{
	}

	/**
	 * Filter called by filterExtension
	 */
	public function filter(float|int|string|null ...$filterData): static
	{
        // phpcs:ignore SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration.MissingVariable
		/** @var array{age: int|null} $filterData */
		$this->filter = $filterData;

		return $this;
	}

	/**
	 * Page called by paginatorExtension
	 */
	public function page(Paginator $paginator): static
	{
		$this->paginator = $paginator;

		// This needs to be called, because of paginator needs info about last page number
		// That means, that in case of pagination the filter will be applied!!!
		// Without filter, paginator will have wrong number of pages!!!
		$data = $this->filter
			? $this->innerFilter(...$this->filter)
			: $this->data;

		$this->paginator->setItemCount(count($data));

		return $this;
	}

	/**
	 * Called by extension.
	 */
	public function orderBy(string $column, OrderDirectionCase $direction): static
	{
		$this->sort = [
			'column' => $column,
			'direction' => $direction,
		];

		return $this;
	}

	/**
	 * Build method should be used in all builders.
	 * This one applies the filter and paginator and returns the data.
	 *
	 * Remember to call the filtering and paginating method and other builder methods in the build method.
	 * This provides you flexibility when you need to change settings, and it is lazy,
	 * so when you don't call the build method, then filtering will not execute.
	 *
	 * In return value you need to provide Countable, ArrayAccess and Traversable at the same time.
	 * Also entities must implement Resource interface, so make sure they are.
	 *
	 * @return Countable&ArrayAccess<int|string, Resource>&Traversable<Resource>
	 */
	public function build(): Traversable&Countable&ArrayAccess
	{
		$data = $this->data;
		// Filter needs to be called first, so we will have only required entities to sort
		if ($this->filter) {
			$data = $this->innerFilter(...$this->filter); // Because of checking data types.
		}

		// After that we need to sort them. When we paginate first, then ordering would be applied only for page.
		$data = $this->innerSort($data);
		// And finally we can paginate the items.
		$data = $this->innerPage($data);

		return ArrayHash::from($data);
	}

	/**
	 * All parameters need to be always nullable. It is for filtering only some values...
	 * Extension will always pass all the arguments you specified in FilterSetupBuilder
	 *
	 * @return array<int|string, Resource>
	 */
	private function innerFilter(int|null $age): array
	{
		if ($age === null) {
			return $this->data;
		}

		// In your implementation will be Resource datatype different, but make sure they implement that interface.
		// FE. by Person&Resource
		/* @phpstan-ignore-next-line */
		return array_filter($this->data, static fn (Resource $person): bool => $person->age === $age);
	}

	/**
	 * Because of laziness
	 *
	 * @param array<int|string, Resource> $data
	 * @return array<int|string, Resource>
	 */
	private function innerPage(array $data): array
	{
		if ($this->paginator === null) {
			return $data;
		}

		$chunks = array_chunk($data, max(1, $this->paginator->getItemsPerPage()), true);

		return $chunks[$this->paginator->getPage() - $this->paginator->getFirstPage()] ?? [];
	}

	/**
	 * Because of laziness
	 *
	 * @param array<int|string, Resource> $data
	 * @return array<int|string, Resource>
	 */
	private function innerSort(array $data): array
	{
		if ($this->sort === null) {
			return $data;
		}

		$column = $this->sort['column'];
		$direction = $this->sort['direction'] === OrderDirectionCase::ASC ? 1 : -1;

		// In your implementation will be Resource datatype different, but make sure they implement that interface.
		// FE. by Person&Resource or more generally Entity&Resource
		uasort($data, static function (Resource $person1, Resource $person2) use ($column, $direction): int {
			$retVal = is_string($person1->$column)
				? strcmp(
					$person1->$column,
					$person2->$column,
				)
				: $person1->$column - $person2->$column;

			return $retVal * $direction;
		});

		return $data;
	}

}
