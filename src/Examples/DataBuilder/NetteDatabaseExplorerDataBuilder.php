<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Examples\DataBuilder;

use ArrayAccess;
use Countable;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Security\Resource;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;
use Nette\Utils\Paginator;
use Snugcomponents\Datagrid\DatagridDataBuilder;
use Snugcomponents\Datagrid\OrderDirectionCase;
use Traversable;
use function sprintf;

class NetteDatabaseExplorerDataBuilder implements DatagridDataBuilder
{

	use SmartObject;

	public function __construct(private readonly Selection $data)
	{
	}

	/**
	 * Filter called by filterExtension
	 */
	public function filter(float|int|string|null ...$filterData): static
	{
		// We can do that immediately, because of Selection is already Builder
		/* @phpstan-ignore-next-line */
		$this->innerFilter(...$filterData); // Because of checking data types.

		return $this;
	}

	/**
	 * Page called by paginatorExtension
	 */
	public function page(Paginator $paginator): static
	{
		$numOfItems = $this->data->count('*');
		$paginator->setItemCount($numOfItems);

		// We can do that immediately, because of Selection is already Builder
		$this->data
			->limit(
				$paginator->getLength(),
				$paginator->getOffset(),
			);

		return $this;
	}

	/**
	 * Called by extension.
	 */
	public function orderBy(string $column, OrderDirectionCase $direction): static
	{
		// We can do that immediately, because of Selection is already Builder
		$this->data->order(sprintf('%s %s', $column, $direction->value));

		return $this;
	}

	/**
	 * Build method should be used in all builders.
	 * This one fetches data and make all of them implement Resource interface.
	 *
	 * To avoid this problem with early fetch use some ORM and make entities implement this interface.
	 *
	 * @return Countable&ArrayAccess<int|string, Resource>&Traversable<Resource>
	 */
	public function build(): Traversable&Countable&ArrayAccess
	{
		$retVal = [];

		foreach ($this->data as $element) {
			/**
			 * @var ActiveRow $element
			 */
			$retVal[] = new class($element) implements Resource {

				public function __construct(private readonly ActiveRow $activeRow)
				{
				}

				public function getResourceId(): string
				{
					return 'person';
				}

				public function __get(string $name): mixed
				{
					return $this->activeRow->$name;
				}

			};
		}

		return ArrayHash::from($retVal);
	}

	/**
	 * All parameters need to be always nullable. It is for filtering only some values...
	 * Extension will always pass all the arguments you specified in FilterSetupBuilder
	 */
	private function innerFilter(int|null $age): void
	{
		if ($age === null) {
			return;
		}

		$this->data->where('age', $age);
	}

}
