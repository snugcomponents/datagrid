<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Examples\EntityManipulator;

use Nette\Security\Resource;
use Snugcomponents\Datagrid\EntityManipulator;

/**
 * This Class is for testing only!
 *
 * It does absolutely nothing
 */
class VoidManipulator implements EntityManipulator
{

	public function persist(Resource $resourceEntity): void
	{
		// TODO: Implement persist() method.
	}

	public function flush(Resource|null $resourceEntity = null): void
	{
		// TODO: Implement flush() method.
	}

	public function remove(Resource $resourceEntity): void
	{
		// TODO: Implement remove() method.
	}

}
