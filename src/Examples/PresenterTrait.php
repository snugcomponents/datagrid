<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Examples;

use Nette\DI\Attributes\Inject;
use Nette\Security\Resource;
use Snugcomponents\Datagrid\Datagrid;
use Snugcomponents\Datagrid\DatagridFactory;
use Snugcomponents\Datagrid\Examples\DataBuilder\ArrayDataBuilder;
use Snugcomponents\Datagrid\Examples\EntityManipulator\VoidManipulator;
use Snugcomponents\Datagrid\InlineEditor\SetupBuilder;
use Snugcomponents\Filter\FilterSetupBuilder;
use function array_map;

/**
 * provides naive implementation of creating the datagrid.
 * It is trait you should use in presenters.
 */
// phpcs:ignore SlevomatCodingStandard.Classes.SuperfluousTraitNaming.SuperfluousSuffix
trait PresenterTrait
{

	#[Inject]
	public DatagridFactory $datagridFactory;

	public function createComponentUserDatagrid(): Datagrid
	{
		if (!$this->user->isAllowed('userDatagrid', 'view')) {
			$this->error('You do not have permission');
		}

		$datagridDataBuilder = new ArrayDataBuilder($this->createResourceArrayFromArray([
			'person1' => [
				'name' => 'josef',
				'age' => 65,
			],
			'person2' => [
				'name' => 'pepa',
				'age' => 41,
			],
			'person3' => [
				'name' => 'karel',
				'age' => 41,
			],
			'person4' => [
				'name' => 'Ondra',
				'age' => 65,
			],
			'person5' => [
				'name' => 'Miluš',
				'age' => 41,
			],
		]));

		$datagrid = $this->datagridFactory->create(new VoidManipulator(), $datagridDataBuilder, 2);
		$datagrid->setFilterSetupBuilder((new FilterSetupBuilder())->addInt('age', 'Věk'));
		$datagrid->setInlineEditorSetupBuilder((new SetupBuilder())->addInt('age', 'Věk'));

		return $datagrid;
	}

	private function createResourceArrayFromArray(array $persons): array
	{
		return array_map($this->createResourceFromArray(...), $persons);
	}

	private function createResourceFromArray(array $person): Resource
	{
		return new class($person) implements Resource {

			public function __construct(private readonly array $row)
			{
			}

			public function getResourceId(): string
			{
				return 'person';
			}

			public function __get(string $name)
			{
				return $this->row[$name];
			}

		};
	}

}
