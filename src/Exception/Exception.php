<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Exception;

use Exception as PhpException;
use Throwable;
use function sprintf;

class Exception extends PhpException
{

	public function __construct(
		ExceptionCase $case,
		string|null $variablePart = null,
		Throwable|null $previous = null,
	)
	{
		match ($case) {
			ExceptionCase::MissingFilterSetupBuilder => parent::__construct(
				'If you want to use filter in your datagrid, you need to set it first by calling method \'setFilterSetupBuilder\'.',
				$case->value,
				$previous,
			),
			ExceptionCase::InvalidArgumentScalar => parent::__construct(
				sprintf('Invalid argument exception. Scalar (int, float, string or bool), %s given.', $variablePart),
				$case->value,
				$previous,
			),
			ExceptionCase::MissingInlineEditorSetupBuilder => parent::__construct(
				'If you want to use inline editing, you need to set it first by calling method \'setInlineEditSetupBuilder\'.',
				$case->value,
				$previous,
			),
		};
	}

}
