<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Exception;

use Exception as PhpException;

// phpcs:ignore SlevomatCodingStandard.Classes.SuperfluousExceptionNaming.SuperfluousSuffix
class DisabledOrderingException extends PhpException
{

}
