<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Exception;

enum ExceptionCase: int
{

	case MissingFilterSetupBuilder = 1;
	case InvalidArgumentScalar = 2;
	case MissingInlineEditorSetupBuilder = 3;

}
