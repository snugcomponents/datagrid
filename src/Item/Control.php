<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Item;

use Nette\Application\UI\Control as UiControl;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Http\IResponse;
use Nette\Security\Resource;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Snugcomponents\Datagrid\EntityManipulator;
use Snugcomponents\Datagrid\Exception\Exception;
use Snugcomponents\Datagrid\Exception\ExceptionCase;
use Snugcomponents\Datagrid\InlineEditor\Control as InlineEditorControl;
use Snugcomponents\Datagrid\InlineEditor\ControlFactory;
use Snugcomponents\Datagrid\InlineEditor\SetupBuilder;
use function assert;

class Control extends UiControl
{

	/** @var array<callable(self): void>  Occurs when remove signal is received */ /** @phpstan-ignore-line */
	public array $onRemove = []; /** @phpstan-ignore-line */

	/* @phpstan-ignore-next-line */
	/** @var array<callable(Form, array|object): void|callable(array|object): void>  Occurs when inline edit form success */
	public array $onInlineEditSuccess = []; /** @phpstan-ignore-line */

	/**
	 * Constructor comment because of PHP stan and codesniffer...
	 */
	public function __construct(
		private readonly ControlFactory $controlFactory,
		private readonly User $user,
		private readonly Resource $resourceEntity,
		private readonly EntityManipulator $entityManipulator,
		private readonly string $templateFile,
		private readonly string $inlineEditorTemplateFile,
		private readonly SetupBuilder|null $setupBuilder = null,
	)
	{
	}

	public function render(): void
	{
		$template = $this->getTemplate();
		assert($template instanceof Template);

		$template->render($this->templateFile, [
			'resourceEntity' => $this->resourceEntity,
		]);
	}

	public function createComponentInlineEditForm(): Multiplier
	{
		if (!$this->setupBuilder) {
			throw new Exception(ExceptionCase::MissingInlineEditorSetupBuilder);
		}

		if (!$this->user->isAllowed($this->resourceEntity, 'edit')) {
			$this->error('You are not allowed to edit this.', IResponse::S401_UNAUTHORIZED);
		}

		$setupBuilder = $this->setupBuilder;
		assert($setupBuilder instanceof SetupBuilder);

		return new Multiplier(function (string $colName) use ($setupBuilder): InlineEditorControl {
			if (!$setupBuilder->hasColumn($colName)) {
				$this->error('Sorry but this field cannot be changed..', IResponse::S401_UNAUTHORIZED);
			}

			return $this->controlFactory->create(
				$this->entityManipulator,
				$setupBuilder,
				$colName,
				$this->resourceEntity,
				$this->inlineEditorTemplateFile,
			);
		});
	}

	public function handleRemove(): void
	{
		if (!$this->user->isAllowed($this->resourceEntity, 'remove')) {
			$this->error('You do not have permission to remove this.', IResponse::S401_UNAUTHORIZED);
		}

		$this->entityManipulator->remove($this->resourceEntity);
		$this->entityManipulator->flush();
		Arrays::invoke($this->onRemove);
	}

}
