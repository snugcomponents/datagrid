<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\Item;

use Nette\Security\Resource;
use Snugcomponents\Datagrid\EntityManipulator;
use Snugcomponents\Datagrid\InlineEditor\SetupBuilder;

interface ControlFactory
{

	public function create(
		Resource $resourceEntity,
		EntityManipulator $entityManipulator,
		string $templateFile,
		string $inlineEditorTemplateFile,
		SetupBuilder|null $setupBuilder = null,
	): Control;

}
