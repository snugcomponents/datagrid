<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid;

interface DatagridFactory
{

	public function create(
		EntityManipulator $entityManipulator,
		DatagridDataBuilder $datagridDataBuilder,
		int $itemsPerPage,
	): Datagrid;

}
