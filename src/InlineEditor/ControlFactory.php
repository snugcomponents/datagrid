<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\InlineEditor;

use Nette\Security\Resource;
use Snugcomponents\Datagrid\EntityManipulator;

interface ControlFactory
{

	public function create(
		EntityManipulator $entityManipulator,
		SetupBuilder $setupBuilder,
		string $colName,
		Resource $resourceEntity,
		string $templateFile,
	): Control;

}
