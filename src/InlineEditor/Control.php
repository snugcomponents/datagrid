<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\InlineEditor;

use Nette\Application\UI\Control as UiControl;
use Nette\Application\UI\Form;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Localization\Translator;
use Nette\Security\Resource;
use Snugcomponents\Datagrid\EntityManipulator;
use function array_merge;
use function assert;

class Control extends UiControl
{

	/* @phpstan-ignore-next-line */
	/** @var array<callable(Form, array|object): void|callable(array|object): void>  Occurs when inline edit form success */
	public array $onSuccess = []; /** @phpstan-ignore-line */

	/**
	 * Constructor comment because of PHP stan and codesniffer...
	 */
	public function __construct(
		private readonly EntityManipulator $entityManipulator,
		private readonly SetupBuilder $setupBuilder,
		private readonly string $colName,
		private readonly Resource $resourceEntity,
		private readonly string $templateFile,
		private readonly Translator|null $translator = null,
	)
	{
	}

	public function render(): void
	{
		$template = $this->getTemplate();
		assert($template instanceof Template);

		$template->render($this->templateFile, [
			'name' => $this->colName,
		]);
	}

	public function createComponentForm(): Form
	{
		$form = $this->setupBuilder->build($this->colName, $this->resourceEntity);
		$form->setTranslator($this->translator);

		/* @phpstan-ignore-next-line */
		$form->onSuccess[] = $this->onSuccess(...);
		/* @phpstan-ignore-next-line */
		$form->onSuccess = array_merge($form->onSuccess, $this->onSuccess);

		return $form;
	}

	/**
	 * @param array<int|string|float|null> $values
	 */
	public function onSuccess(array $values): void
	{
		$this->resourceEntity->{$this->colName} = $values[$this->colName];
		$this->entityManipulator->persist($this->resourceEntity);
		$this->entityManipulator->flush();
	}

}
