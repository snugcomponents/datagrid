<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\InlineEditor;

use Nette\Application\UI\Form;
use Nette\Forms\Controls\TextBase;
use Nette\Security\Resource;
use Nette\SmartObject;
use Snugcomponents\Datagrid\Exception\Exception;
use Snugcomponents\Filter\InputType;
use function assert;

class SetupBuilder
{

	use SmartObject;

	/** @var array<Setup> */
	private array $setups = [];

	final public function __construct()
	{
		// Empty constructor because of create method and its new static() call
	}

	public static function create(): static
	{
		return new static();
	}

	public function addText(string $name, string $caption, bool $nullable = false): static
	{
		$this->setups[$name] = new Setup(
			caption: $caption,
			type: InputType::Text,
			nullable: $nullable,
		);

		return $this;
	}

	public function addInt(string $name, string $caption, bool $nullable = false): static
	{
		$this->setups[$name] = new Setup(
			caption: $caption,
			type: InputType::Int,
			nullable: $nullable,
		);

		return $this;
	}

	public function addFloat(string $name, string $caption, bool $nullable = false): static
	{
		$this->setups[$name] = new Setup(
			caption: $caption,
			type: InputType::Float,
			nullable: $nullable,
		);

		return $this;
	}

	public function addDatetime(string $name, string $caption, bool $nullable = false): static
	{
		$this->setups[$name] = new Setup(
			caption: $caption,
			type: InputType::Datetime,
			nullable: $nullable,
		);

		return $this;
	}

	/**
	 * @param array<int|string, scalar> $items
	 * @throws Exception
	 */
	public function addSelect(string $name, string $caption, array $items = []): static
	{
		$this->setups[$name] = new SetupSelect(
			caption: $caption,
			type: InputType::Select,
			items: $items,
		);

		return $this;
	}

	/**
	 * @param array<int|string, scalar> $items
	 * @throws Exception
	 */
	public function addMultiSelect(string $name, string $caption, array $items = []): static
	{
		$this->setups[$name] = new SetupSelect(
			caption: $caption,
			type: InputType::MultiSelect,
			items: $items,
		);

		return $this;
	}

	public function hasColumn(string $colName): bool
	{
		return isset($this->setups[$colName]);
	}

	public function build(string $name, Resource $resourceEntity): Form
	{
		$form = new Form();
		$form->getElementPrototype()->setAttribute('novalidate', 'novalidate');

		$setup = $this->setups[$name];

		$input = null;

		match ($setup->type) {
			InputType::Text => $input = $form->addText($name, $setup->caption),
			InputType::Int => $input = $form->addInteger($name, $setup->caption),
			InputType::Float => $input = $form->addText($name, $setup->caption)->addRule(
				$form::FLOAT,
			)->setHtmlType('float'),
			InputType::Datetime => $input = $form->addText($name, $setup->caption)->setHtmlType(
				'datetime',
			),
			InputType::Select => $input = $form->addSelect(
				$name,
				$setup->caption,
				$this->getItemsFromSelect($setup),
			)->setPrompt(''),
			InputType::MultiSelect => $input = $form->addMultiSelect(
				$name,
				$setup->caption,
				$this->getItemsFromSelect($setup),
			),
		};

		if ($input instanceof TextBase) {
			$input->setNullable($setup->nullable);
		}

		/* @phpstan-ignore-next-line */
		$input->setDefaultValue($resourceEntity->$name);

		return $form;
	}

	/**
	 * @return array<int|string, scalar>
	 */
	private function getItemsFromSelect(Setup $setupSelect): array
	{
		assert($setupSelect instanceof SetupSelect);

		return $setupSelect->items;
	}

}
