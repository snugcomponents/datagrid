<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\InlineEditor;

use Nette\SmartObject;
use Snugcomponents\Filter\InputType;

class Setup
{

	use SmartObject;

	public function __construct(
		public string $caption,
		public InputType $type,
		public bool $nullable = false,
	)
	{
	}

}
