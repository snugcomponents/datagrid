<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid\InlineEditor;

use Snugcomponents\Datagrid\Exception\Exception;
use Snugcomponents\Datagrid\Exception\ExceptionCase;
use Snugcomponents\Filter\InputType;
use function gettype;
use function is_scalar;

class SetupSelect extends Setup
{

	/**
	 * @param array<int|string, scalar> $items
	 * @throws Exception
	 */
	public function __construct(
		string $caption,
		InputType $type,
		public array $items,
		bool $nullable = false,
	)
	{
		foreach ($items as $key => $item) {
			if (!is_scalar($item)) {
				throw new Exception(ExceptionCase::InvalidArgumentScalar, gettype($key));
			}
		}

		parent::__construct($caption, $type, $nullable);
	}

}
