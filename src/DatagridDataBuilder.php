<?php declare(strict_types = 1);

namespace Snugcomponents\Datagrid;

use ArrayAccess;
use Countable;
use Nette\Security\Resource;
use Snugcomponents\Datagrid\Exception\DisabledOrderingException;
use Snugcomponents\Filter\FilterDataBuilder;
use Snugcomponents\Paginator\PaginatorDataBuilder;
use Traversable;

interface DatagridDataBuilder extends FilterDataBuilder, PaginatorDataBuilder
{

	/**
	 * @param string $column                Name of column/property to sort
	 * @param OrderDirectionCase $direction      Direction of sorting.
	 * @throws DisabledOrderingException        Appears when disabled or non-existing $column is provided.
	 */
	public function orderBy(string $column, OrderDirectionCase $direction): static;

	/**
	 * @return Countable&ArrayAccess<int|string, Resource>&Traversable<Resource>
	 */
	public function build(): Traversable&Countable&ArrayAccess;

}
