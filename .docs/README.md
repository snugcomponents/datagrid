# SnugComponents / Datagrid control

## Content
<!-- TOC -->
- [Content](#content)
- [Setup](#setup)
- [Usage](#usage)
    * [Injecting/using](#injecting/using)
- [Setting](#setting)
    * [Own templates](#own-templates)
    * [Translations](#translations)
    * [Ajax](#ajax)
    * [Laziness](#laziness)
    * [Ordering](#ordering)
    * [Filtering](#filtering)
    * [Pagination](#pagination)
    * [Inline editing](#inline-editing)
    * [Own callbacks](#own-callbacks)
    * [Security](#security)
- [Examples](#examples)
<!-- TOC -->
## Setup

This extension depends on other two extensions. Specifically on:

`snugcomponents/filter`

`snugcomponents/paginator`

So make sure that they are registered, and you read their manual too.

```neon
extensions:
	SnugcomponentsPaginator: Snugcomponents\Paginator\PaginatorExtension
	SnugcomponentsFilter: Snugcomponents\Filter\FilterExtension
	SnugcomponentsDatagrid: Snugcomponents\Datagrid\DatagridExtension
```

## Usage

### Injecting/using

You can simply inject factory in Your Presenters/Controls:

```php
public function __construct(
    private readonly \Snugcomponents\Datagrid\DatagridFactory $datagridFactory,
) {
    parent::__construct();
    ...
}
```
And then use it: (minimal setting)

```php
public function createComponentDatagrid(): \Snugcomponents\Datagrid\Datagrid
{
    return $this->datagridFactory->create(
        entityManipulator: $this->entityManipulator, // Instance of EntityManipulator
        datagridDataBuilder: $this->dataBuilder,     // Instance of DatagridDataBuilder
        itemsPerPage: 20,                            // Items per page (Paginator always counts)
    );
}
```

## Setting

This extension has lots of setting, so make sure that you use it when you need it.

### Own templates

If the original templates does not meet your needs (they don't), then you can paste your own template into the components by calling several methods.
Each of them are called on the `Datagrid` instance and needs to be `.latte` file.

`setTemplateFile` sets main template for datagrid. (Like a table) [example](../src/Examples/templates/bootstrap4.latte)

`setItemTemplateFile` sets template used for one item. (Like a table row) [example](../src/Examples/templates/Item/bootstrap4.latte)

`setPaginatorTemplateFile` sets template to paginator. see [paginator doc.](https://gitlab.com/snugcomponents/paginator/-/tree/HEAD/.docs)

`setFilterTemplateFile` sets template to filter. see [filter doc.](https://gitlab.com/snugcomponents/filter/-/tree/HEAD/.docs)

`setInlineEditorTemplateFile` sets template for inline editing form (Like a table col) [example](../src/Examples/templates/InlineEditor/bootstrap4.latte)

For inspiration and understanding template structure go to [Examples](../src/Examples/templates) folder.

### Translations

This extension automatically translates all forms and your own templates.
Only thing what you need to do is to register your translator, which implements `Nette\Localization\Translator` interface as a service.
If you need to change default Button caption or clear filter link text, then you need to provide your own template.

### Ajax

This extension fully supports ajax requests. Please feel free to use any js ajax library.

### Laziness

This extension is lazy like an author. It uses laziness of other extensions and provides more itself.
Only other component created together is Paginator.

If you do not use filter or inline editing, they don't even create themselves, so it's safe processor time.

### Ordering

Ordering is always enabled. How to create ordering links see [example tamplate.](../src/Examples/templates/bootstrap4.latte)
In default, all columns/properties are sortable. If you don't want to order by some columns,
or you want to throw [404 - not found](https://cs.wikipedia.org/wiki/HTTP_404) error when non-existing column is provided,
you need to handle that in your `DatagridDataBuilder` implementation and throw `DisabledOrderingException` in the `orderBy` function.

### Filtering

To turn on filtering, you need to call `setFilterSetupBuilder` on `Datagrid` instance. 
For info about `FilterSetupBuilder` see the [filter doc.](https://gitlab.com/snugcomponents/filter/-/tree/HEAD/.docs)

In Datagrid template you can render filter by typing `{control filter}`

### Pagination

Pagination does not need any setting. Ony required info is how many items you need on one page.
This info you need to provide by `Datagrid` constructor.

In Datagrid template you can render paginator by typing `{control paginator}`

### Inline editing

Inline editing can be enabled by calling `setInlineEditorSetupBuilder` on `Datagrid` instance.
This `SetupBuilder`, which you will need as an argument has the same functionality as FilterSetupBuilder.

### Own callbacks

Every single action has defined their own callbacks. But if you need some extra callbacks, 
you can set them easily by add your callback to the callback array. All of them are in `Datagrid` instance.

Possible callbacks:

`$onRemove[]` Occurs when remove signal is received

`$onPagination[]` Occurs when page signal is received

`$onFiltering[]` Occurs when filter signal is received (even if filter is cleared)

`$onOrdering[]` Occurs when ordering signal is received

`$onInlineEditSuccess[]` Occurs when inline edit form success

### Security

This extension is fully secured. To do so Datagrid requires that all entities needs to implement `Nette\Security\Resource` interface.
On top of that you need to use an authorizator and Nette identity to recognize the privilege.

Only 2 privileges are checked: `edit` and `remove`. If user, who don't have permission try to perform these actions, then [401](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#401) will be returned.

## Examples

In examples folder you can find implementation of all stuff that you will need. 
In templates, you can even find all used properties, controls and snippets.

You need to provide your own implementation of DatagridDataBuilder and EntityManipulator.
You already know where to find the examples.

There is also example of PresenterTrait, which creates a datagrid with full settings.

Have fun.
