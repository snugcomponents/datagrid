## Installation

To install the latest version of `snugcomponents/datagrid` use [Composer](https://getcomposer.org).

```bash
composer require snugcomponents/datagrid
```

## Documentation

For details on how to use this package, check out our [documentation](.docs).

## Versions

| State  | Version  | Branch | Nette | PHP     |
|--------|----------|--------|-------|---------|
| stable | `^1.0.4` | `main` | 3.1+  | `>=8.1` |

## Development

This package is currently maintaining by these authors.

<a href="https://gitlab.com/tondajehlar">
  <img width="80" height="80" src="https://avatars2.githubusercontent.com/u/9120518?v=3&s=80">
</a>

-----

## Conclusion
This package requires PHP 8.1, Nette 3.1, and it is property of SnugDesign © 2022
